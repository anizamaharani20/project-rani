<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" 
    integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link href="https://fonts.googleapis.com/css2?family=Amita&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Alex Brush" rel="stylesheet">
    
    <title>GO-CAMP</title>
  </head>
  <body id="page-top">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
    <a class="navbar-brand font-weight-bold text-black" href="#page-top" href="/togamedia">GO-CAMP</a>
    <img src="img/4.png" alt="" width="70px" height="70px">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" 
  data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item active">
        <a class="nav-link js-scroll-trigger font-weight-bold text-black" 
          href="?page=welcome">About<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link js-scroll-trigger font-weight-bold text-black" 
          href="?page=tempat" aria-disabled="true">Location</a>
      </li>
      <li class="nav-item">
        <a class="nav-link js-scroll-trigger font-weight-bold text-black" 
          href="?page=kategori" aria-disabled="true">Contact</a>
      </li>
    </ul>
  </div>
  </div>
</nav>

<div class="jumbotron">
 <div class="container">
    <h1 class="display-4"><span class="font-weight-bold">WELCOME
    <br>ENJOY YOUR ADVENTURE</span><br><span class="font-weight-bold">WITH GO-CAMP</span><br></h1>
    <!-- <p>Make your holiday memorable and unforgettable</p> -->
    <a class="btn btn-primary btn-lg font-weight-bold" href="view/v_login.php" role="button">FIND OUT MORE</a>
 </div>
</div>

<!-- CONTENT -->
<div class="content">
  <center><h1>Welcome to Go-Camp</h1></center>
  <div class="isi">
    <p>Go-Camp is a service that provides borrowing camping equipment.
        Go-Camp strives to always provide a pleasant service for its customers,
        Go-Camp is different from others because the principle of Go-Camp is to provide
        quality holiday supplies at low prices. With Go-Camp you can make your vacation
        an experience that will never be forgotten.</p>
  </div>

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="img/3.jpg" class="d-block w-100" alt="...">
        <div class="carousel-caption d-none d-md-block">
        <h1 class="display-4"><span class="font-weight-bold">RAJA AMPAT</span>
        <br><span class="font-weight-bold">WITH GO-CAMP</span></h1>
        <p>Go-Camp can make your vacation an experience that will never be forgotten.</p>
      </div>
      </div>
      <div class="carousel-item">
        <img src="img/dalat.jpg" class="d-block w-100" alt="...">
        <h1 class="display-4"><span class="font-weight-bold"></span><br><span class="font-weight-bold"></span></h1>
        <p></p>
      </div>
      <div class="carousel-item">
        <img src="img/bali.jpg" class="d-block w-100" alt="...">
        <h1 class="display-4"><span class="font-weight-bold"></span><br><span class="font-weight-bold"></span></h1>
        <p></p>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>
<div class="copyright text-center text-white font-weight-bold bg-dark p-2">
  <p>Copyright © 2021 Go-Camp Hiking & Camping - Outdoor Equipment Rental</p>
</div>

  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" 
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

  </body>
</html>