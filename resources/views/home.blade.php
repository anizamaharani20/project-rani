<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('images/auth/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('images/auth/camping.png')}}">
  <title>
   GO-CAMP
  </title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <!-- Fonts and icons -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
  <!-- Nucleo Icons -->
  <link href="{{asset('css/nucleo-icons.css')}}" rel="stylesheet" />
  <link href="{{asset('css/nucleo-svg.css')}}" rel="stylesheet" />
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
  <link href="{{asset('css/nucleo-svg.css')}}" rel="stylesheet" />
  <!-- CSS Files -->

  <style>
    .card-body-icon{
      position: absolute;
      z-index: 0;
      top: 10px;
      right: 30px;
      opacity: 0,4;
      font-size: 90px;
    }
  </style>
  
</head>

@section('js')
<script type="text/javascript">
  $(document).ready(function() {
    $('#table').DataTable({
      "iDisplayLength": 50
    });

} );
</script>
@stop
@extends('layouts.app')

@section('content')
@if(Auth::user()->level == 'user')
<div class="row">
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
            <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-account-switch text-success icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Transaksi</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0">{{$transaksi->count()}}</h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-account-switch mr-1" aria-hidden="true"></i> Total seluruh transaksi
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-timer-sand text-success icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Sedang Pinjam</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0">{{$transaksi->where('status', 'pinjam')->count()}}</h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-timer-sand mr-1" aria-hidden="true"></i> sedang dipinjam
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics">
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-nature text-success icon-lg" style="width: 40px;height: 40px;"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Alat</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0">{{$alat->count()}}</h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0">
                    <i class="mdi mdi-hospital mr-1" aria-hidden="true"></i> Total Alat Camping
                  </p>
                </div>
              </div>
            </div>
          </div>
          @endif

          @if(Auth::user()->level == 'admin')
          <div class="col-md-3 p-2 pt-2">
            <h3><i class="fas fa-tachometer-alt mr-2"></i>DASHBOARD</h3>
          </div>
          <div class="row text-white">
            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                    <div class="card bg-info" style="width: 18rem;">
                        <div class="card-body">
                          <div class="card-body-icon">
                            <i class="mdi mdi-cash-multiple text-success icon-lg"></i>
                          </div>
                          <h4 class="card-title">Today's Money</h4>
                          <div class="display-4">$53,000</div>
                          <p class="card-text">+4,000 more</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                      <div class="card bg-success" style="width: 18rem;">
                        <div class="card-body">
                          <div class="card-body-icon">
                            <i class="mdi mdi-account-multiple text-success icon-lg"></i>
                          </div>
                          <h4 class="card-title">Today's Users</h4>
                          <div class="display-4">2,300</div>
                          <p class="card-text">+5,3%</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                      <div class="card bg-warning" style="width: 18rem;">
                        <div class="card-body">
                          <div class="card-body-icon">
                            <i class="mdi mdi-account-location text-success icon-lg"></i>
                          </div>
                          <h4 class="card-title">New Client</h4>
                          <div class="display-4">+3,462</div>
                          <p class="card-text">+34 last week</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                      <div class="card bg-danger" style="width: 18rem;">
                        <div class="card-body">
                          <div class="card-body-icon">
                            <i class="mdi mdi-cart text-success icon-lg"></i>
                          </div>
                          <h4 class="card-title">Transaction</h4>
                          <div class="display-4">$103,430</div>
                          <p class="card-text">+6.7%</p>
                        </div>
                      </div>
                    </div>
          @endif
        </div>
      </div>
    </div>
  </div>
@endsection

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>