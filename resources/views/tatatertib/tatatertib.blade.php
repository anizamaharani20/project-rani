@section('js')
<script type="text/javascript">
  $(document).ready(function() {
    $('#table').DataTable({
      "iDisplayLength": 50
    });

} );
</script>
@stop
@extends('layouts.app')

@section('content')
<div class="row">

</div>
<div class="row" style="margin-top: 20px;">
<div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                <h1>Selamat Datang di GO-CAMP Official</h1>
                  <h3>PERATURAN & TATA TERTIB PERSEWAAN</h3>
                  <img style="margin-right:60px;" class="rounded float-left" width="250px" height="250px" src="{{asset('images/auth/camping.png')}}">
                  <div>
                  <ol style="font-size: 20px;">
                      <li>Meninggalkan kartu identitas yang masih berlaku (KTP/KTM/Kartu Pelajar)</li>
                      <li>Penyewa wajib mengecek semua perlengkapan yang akan disewa</li>
                      <li>Peralatan persewaan dalam kondisi baik dan utuh, penyewa wajib merawat dan menjaga dengan baik</li>
                      <li>Jika ada kerusakan/kehilangan, penyewa wajib membayar sesuai kerusakan atau menggantinya</li>
                      <li>Segala bentuk penipuan akan kami serahkan pada pihak yang berwajib</li>
                    </ol>
                </div>
                </div>
                </div>
              </div>
            </div>
          </div>
@endsection