<?php

use Illuminate\Database\Seeder;

class AlatsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Alat::insert([
            [
              'id'  			=> 1,
              'nama_alat'  		=> 'Tenda Camping Double Layer',
              'harga'           => '590000',
              'jumlah_alat'		=> 20,
              'deskripsi'		=> 'Sangat cocok untuk kegiatan camping, outdoor, maupun hiking, kapasitas 4-5 orang',
              'foto_alat'		=> '1.jpg',
              'created_at'      => \Carbon\Carbon::now(),
              'updated_at'      => \Carbon\Carbon::now()
            ],
            [
                'id'  			=> 2,
                'nama_alat'  	=> 'Head Lamp Charge Camping',
                'harga'         => '80000',
                'jumlah_alat'	=> 50,
                'deskripsi'		=> 'Dilengkapi dengan Strap Kepala yang nyaman dipakai, tetap kuat dan elastis',
                'foto_alat'		=> '3.jpg',
                'created_at'    => \Carbon\Carbon::now(),
                'updated_at'    => \Carbon\Carbon::now()
            ],
            [
                'id'  			=> 3,
                'nama_alat'  	=> 'Sleeping Bag',
                'harga'         => '160000',
                'jumlah_alat'	=> 15,
                'deskripsi'		=> 'Waterproof comfortable keep warming and simple design style',
                'foto_alat'		=> '10.jpg',
                'created_at'    => \Carbon\Carbon::now(),
                'updated_at'    => \Carbon\Carbon::now()
            ],
            [
                'id'  			=> 4,
                'nama_alat'  	=> 'Tas Carier 60 Liter',
                'harga'         => '240000',
                'jumlah_alat'	=> 25,
                'deskripsi'		=> 'Bahan cordura 1000 combinasi ristop, di lengkapi raincover anti hujan',
                'foto_alat'		=> '79493-2019-11-23-08-16-01.jpg',
                'created_at'    => \Carbon\Carbon::now(),
                'updated_at'    => \Carbon\Carbon::now()
            ],
        ]);
    }
}
